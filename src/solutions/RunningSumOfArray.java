package solutions;

/**
 * Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
 *
 * Return the running sum of nums.
 *
 * DO NOTE! no testcases yet written.
 */
public class RunningSumOfArray {

    public static void main(String[] args) {
        int[] numbers = new int[]{1,2,3,4};
        int[] numbers2 = new int[]{1,1,1,1,1};
        int[] numbers3 = new int[]{3,2,3,4,2,5};
        int[] numbers4 = new int[]{1,2,3,4,7,1,2};

        runningSum(numbers);
        runningSum(numbers2);
        runningSum(numbers3);
        runningSum(numbers4);
    }

    public static int[] runningSum(int[] nums){
        int[] sumArray = new int[nums.length];
        int total = 0;
        for (int n = 0; n<nums.length; n++) {
            total += nums[n];
            sumArray[n] = total;
        }
        return sumArray;
    }
}
