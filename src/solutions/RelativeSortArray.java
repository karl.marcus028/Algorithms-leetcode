package solutions;

/**
 *
 * Given two arrays arr1 and arr2, the elements of arr2 are distinct, and all elements in arr2 are also in arr1.
 * Sort the elements of arr1 such that the relative ordering of items in arr1 are the same as in arr2.
 * Elements that don't appear in arr2 should be placed at the end of arr1 in ascending order.
 */


import java.util.Arrays;
import java.util.TreeMap;

public class RelativeSortArray {

    public static void main(String[] args) {
        int[] arr1 = new int[]{2,3,1,3,2,4,6,7,9,2,19};
        int[] arr2 = new int[]{2,1,4,3,9,6};

        relativeSortArray1(arr1, arr2);
    }


    //half of the speed of the below solution with nested for-loops
    public static int[] relativeSortArray1(int[] arr1, int[] arr2) {
        TreeMap<Integer, Integer> mappedValues = new TreeMap<>();

        for(int i: arr1) {
            if(mappedValues.containsKey(i)) {
                mappedValues.put(i, mappedValues.get(i) + 1);
            } else {
                mappedValues.put(i, 1);
            }
        }

        int i = 0;
        for(int k: arr2) {
            if(mappedValues.containsKey(k)) {
                for(int j = 0; j < mappedValues.get(k); j++) {
                    arr1[i++] = k;
                }
                mappedValues.put(k, -1);
            }
        }

        for(int k: mappedValues.keySet()) {
            if(mappedValues.get(k) != -1) {
                for(int j = 0; j < mappedValues.get(k); j++) {
                    arr1[i++] = k;
                }
            }
        }

        return arr1;
    }


    //slow performance around 6 ms
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        int count = 0;

        for(int i = 0; i<arr2.length; i++)
        {
            for (int j = count; j<arr1.length; j++) {
              if(arr2[i] == arr1[j]){
                int tempNum = arr1[j];
                arr1[j] = arr1[count];
                arr1[count++] = tempNum;
              }
            }
        }

        //take care of numbers that are not with the length of arr2
        if(count != arr1.length){
            Arrays.sort(arr1, count, arr1.length);
        }

        return arr1;
    }
}
